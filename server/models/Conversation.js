const mongoose = require('mongoose');
const { Schema } = mongoose;
const Message = require('./Message');

const conversationSchema = new mongoose.Schema(
  {
    participants: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    messages: [Message],
  },
  { timestamps: true },
);

const Conversation = mongoose.model('Conversation', conversationSchema);

module.exports = Conversation;
