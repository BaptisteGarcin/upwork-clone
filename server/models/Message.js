const mongoose = require('mongoose');
const { Schema } = mongoose;

const messageSchema = new mongoose.Schema(
  {
    from: { type: Schema.Types.ObjectId, ref: 'User' },
    text: String,
  },
  { timestamps: true },
);

const Message = mongoose.model('Message', messageSchema);

module.exports = messageSchema;
