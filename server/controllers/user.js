const _ = require('lodash');
const { promisify } = require('util');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const passport = require('passport');
const sharp = require('sharp');
const User = require('../models/User');
const randomBytesAsync = promisify(crypto.randomBytes);
const mongoose = require('mongoose');
const { ObjectId } = mongoose.Types;
/**
 * POST /login
 * Sign in using email and password.
 */
exports.postLogin = (req, res, next) => {
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('password', 'Password cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({ gmail_remove_dots: false });
  res.setHeader('Content-Type', 'text/html');

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    // return res.redirect('/logging');
  }

  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      req.flash('errors', info);
      // return res.redirect('/logging');
    }
    req.logIn(user, err => {
      if (err) {
        return next(err);
      }
      User.findByIdAndUpdate(user._id, {
        $set: { disabled: false },
      });
      req.flash('success', { msg: 'Success! You are logged in.' });
      res.send({ redirect: '/profile' });
      // res.redirect(req.session.returnTo || '/');
    });
  })(req, res, next);
};

/**
 * GET /logout
 * Log out.
 */
exports.logout = (req, res) => {
  req.logout();
  req.session.destroy(err => {
    if (err)
      console.log('Error : Failed to destroy the session during logout.', err);
    req.user = null;
    res.redirect('/');
  });
};

/**
 * GET /signup
 * Signup page.
 */
exports.getSignup = (req, res) => {
  if (req.user) {
    return res.redirect('/');
  }
  res.render('account/signup', {
    title: 'Create Account',
  });
};

/**
 * POST /signup
 * Create a new local account.
 */
exports.postSignup = (req, res, next) => {
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req
    .assert('confirmPassword', 'Passwords do not match')
    .equals(req.body.password);
  req.sanitize('email').normalizeEmail({ gmail_remove_dots: false });

  const errors = req.validationErrors();
  console.log(errors);
  if (errors) {
    req.flash('errors', errors);
    // return res.redirect('/register');
  }

  const user = new User({
    email: req.body.email,
    password: req.body.password,
    vaqqerid: Math.floor(Math.random() * 10000000000000),
    disabled: false,
    km: true,
  });

  User.findOne({ email: req.body.email }, (err, existingUser) => {
    if (err) {
      return next(err);
    }
    if (existingUser) {
      req.flash('errors', {
        msg: 'Account with that email address already exists.',
      });
      // return res.redirect('/register');
    }
    user.save(err => {
      if (err) {
        return next(err);
      }
      req.logIn(user, err => {
        if (err) {
          return next(err);
        }
        res.send({ redirect: '/profile' });
        // res.redirect('/');
      });
    });
  });
};

/**
 * POST /account/profile
 * Update profile information.
 */
exports.postUpdateProfile = (req, res, next) => {
  req.assert('email', 'Please enter a valid email address.').isEmail();
  req.sanitize('email').normalizeEmail({ gmail_remove_dots: false });

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/account');
  }

  User.findById(req.user.id, (err, user) => {
    if (err) {
      return next(err);
    }
    user.email = req.body.email || '';
    user.profile.name = req.body.name || '';
    user.profile.gender = req.body.gender || '';
    user.profile.location = req.body.location || '';
    user.profile.website = req.body.website || '';
    user.save(err => {
      if (err) {
        if (err.code === 11000) {
          req.flash('errors', {
            msg:
              'The email address you have entered is already associated with an account.',
          });
          return res.redirect('/account');
        }
        return next(err);
      }
      req.flash('success', { msg: 'Profile information has been updated.' });
      res.redirect('/account');
    });
  });
};

/**
 * POST /account/password
 * Update current password.
 */
exports.postUpdatePassword = (req, res, next) => {
  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req
    .assert('confirmPassword', 'Passwords do not match')
    .equals(req.body.password);

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/account');
  }

  User.findById(req.user.id, (err, user) => {
    if (err) {
      return next(err);
    }
    user.password = req.body.password;
    user.save(err => {
      if (err) {
        return next(err);
      }
      req.flash('success', { msg: 'Password has been changed.' });
      res.redirect('/account');
    });
  });
};

/**
 * POST /account/delete
 * Delete user account.
 */
exports.postDeleteAccount = (req, res, next) => {
  User.findByIdAndDelete(req.session.passport.user, err => {
    if (err) {
      return next(err);
    }
    req.logout();
    req.flash('info', { msg: 'Your account has been deleted.' });
    res.redirect('/');
  });
};

/**
 * GET /account/unlink/:provider
 * Unlink OAuth provider.
 */
exports.getOauthUnlink = (req, res, next) => {
  const { provider } = req.params;
  User.findById(req.user.id, (err, user) => {
    if (err) {
      return next(err);
    }
    user[provider] = undefined;
    user.tokens = user.tokens.filter(token => token.kind !== provider);
    user.save(err => {
      if (err) {
        return next(err);
      }
      req.flash('info', { msg: `${provider} account has been unlinked.` });
      res.redirect('/account');
    });
  });
};

/**
 * GET /reset/:token
 * Reset Password page.
 */
exports.getReset = (req, res, next) => {
  if (req.isAuthenticated()) {
    return res.redirect('/');
  }
  User.findOne({ passwordResetToken: req.params.token })
    .where('passwordResetExpires')
    .gt(Date.now())
    .exec((err, user) => {
      if (err) {
        return next(err);
      }
      if (!user) {
        req.flash('errors', {
          msg: 'Password reset token is invalid or has expired.',
        });
        return res.redirect('/forgot');
      }
      res.render('account/reset', {
        title: 'Password Reset',
      });
    });
};

/**
 * POST /reset/:token
 * Process the reset password request.
 */
exports.postReset = (req, res, next) => {
  req.assert('password', 'Password must be at least 4 characters long.').len(4);
  req.assert('confirm', 'Passwords must match.').equals(req.body.password);

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }

  const resetPassword = () =>
    User.findOne({ passwordResetToken: req.params.token })
      .where('passwordResetExpires')
      .gt(Date.now())
      .then(user => {
        if (!user) {
          req.flash('errors', {
            msg: 'Password reset token is invalid or has expired.',
          });
          return res.redirect('back');
        }
        user.password = req.body.password;
        user.passwordResetToken = undefined;
        user.passwordResetExpires = undefined;
        return user.save().then(
          () =>
            new Promise((resolve, reject) => {
              req.logIn(user, err => {
                if (err) {
                  return reject(err);
                }
                resolve(user);
              });
            }),
        );
      });

  const sendResetPasswordEmail = user => {
    if (!user) {
      return;
    }
    let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
    const mailOptions = {
      to: user.email,
      from: 'your@email.com',
      subject: 'Your password has been changed',
      text: `Hello,\n\nThis is a confirmation that the password for your account ${
        user.email
      } has just been changed.\n`,
    };
    return transporter
      .sendMail(mailOptions)
      .then(() => {
        req.flash('success', {
          msg: 'Success! Your password has been changed.',
        });
      })
      .catch(err => {
        if (err.message === 'self signed certificate in certificate chain') {
          console.log(
            'WARNING: Self signed certificate in certificate chain. Retrying with the self signed certificate. Use a valid certificate if in production.',
          );
          transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
              user: process.env.EMAIL_USERNAME,
              pass: process.env.EMAIL_PASSWORD,
            },
            tls: {
              rejectUnauthorized: false,
            },
          });
          return transporter.sendMail(mailOptions).then(() => {
            req.flash('success', {
              msg: 'Success! Your password has been changed.',
            });
          });
        }
        console.log(
          'ERROR: Could not send password reset confirmation email after security downgrade.\n',
          err,
        );
        req.flash('warning', {
          msg:
            'Your password has been changed, however we were unable to send you a confirmation email. We will be looking into it shortly.',
        });
        return err;
      });
  };

  resetPassword()
    .then(sendResetPasswordEmail)
    .then(() => {
      if (!res.finished) res.redirect('/');
    })
    .catch(err => next(err));
};

/**
 * GET /forgot
 * Forgot Password page.
 */
exports.getForgot = (req, res) => {
  if (req.isAuthenticated()) {
    return res.redirect('/');
  }
  res.render('account/forgot', {
    title: 'Forgot Password',
  });
};

/**
 * POST /forgot
 * Create a random token, then the send user an email with a reset link.
 */
exports.postForgot = (req, res, next) => {
  req.assert('email', 'Please enter a valid email address.').isEmail();
  req.sanitize('email').normalizeEmail({ gmail_remove_dots: false });

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/forgotPass');
  }

  const createRandomToken = randomBytesAsync(16).then(buf =>
    buf.toString('hex'),
  );

  const setRandomToken = token =>
    User.findOne({ email: req.body.email }).then(user => {
      if (!user) {
        req.flash('errors', {
          msg: 'Account with that email address does not exist.',
        });
      } else {
        user.passwordResetToken = token;
        user.passwordResetExpires = Date.now() + 3600000; // 1 hour
        user = user.save();
      }
      return user;
    });

  const sendForgotPasswordEmail = user => {
    if (!user) {
      return;
    }
    const token = user.passwordResetToken;
    let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
    const mailOptions = {
      to: user.email,
      from: 'your@email.com',
      subject: 'Reset your password',
      text: `You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n
        Please click on the following link, or paste this into your browser to complete the process:\n\n
        http://${req.headers.host}/reset/${token}\n\n
        If you did not request this, please ignore this email and your password will remain unchanged.\n`,
    };
    /*eslint-disable */

    return transporter
      .sendMail(mailOptions)
      .then(() => {
        req.flash('info', {
          msg: `An e-mail has been sent to ${
            user.email
          } with further instructions.`,
        });
      })
      .catch(err => {
        if (err.message === 'self signed certificate in certificate chain') {
          console.log(
            'WARNING: Self signed certificate in certificate chain. Retrying with the self signed certificate. Use a valid certificate if in production.',
          );
          transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
              user: process.env.EMAIL_USERNAME,
              pass: process.env.EMAIL_PASSWORD,
            },
            tls: {
              rejectUnauthorized: false,
            },
          });
          return transporter.sendMail(mailOptions).then(() => {
            req.flash('info', {
              msg: `An e-mail has been sent to ${
                user.email
              } with further instructions.`,
            });
          });
        }
        console.log(
          'ERROR: Could not send forgot password email after security downgrade.\n',
          err,
        );
        req.flash('errors', {
          msg:
            'Error sending the password reset message. Please try again shortly.',
        });
        return err;
      });
        /* eslint-enable */
  };

  createRandomToken
    .then(setRandomToken)
    .then(sendForgotPasswordEmail)
    .then(() => res.redirect('/forgotPass'))
    .catch(next);
};

/**
 * GET /api/users
 * Return all users.
 */
exports.getAllUsers = (req, res) => {
  User.find({ disabled: { $in: [false, null] } }).then(users => {
    if (req.query.searchByLocation) {
      const usersWithDistance = _.map(users, user => {
        if (user.profile && user.profile.latlng[0] && user.profile.latlng[1]) {
          const kmTo = distance(
            req.query.searchByLocation[0],
            req.query.searchByLocation[1],
            user.profile.latlng[0],
            user.profile.latlng[1],
          );
          const userWithDistance = JSON.parse(JSON.stringify(user));
          userWithDistance.profile.distance = kmTo;
          return userWithDistance;
        }
        return user;
      });
      const sortedByDistance = _.sortBy(usersWithDistance, [
        'profile.distance',
      ]);
      const sortedByDistanceExceptConnected = sortedByDistance.filter(
        user => user._id != req.session.passport.user,
      );
      res.send(sortedByDistanceExceptConnected);
    } else {
      const allUsersExceptConnected = users.filter(
        user => user._id != req.session.passport.user,
      );
      res.send(allUsersExceptConnected);
    }
  });
};

function distance(lat1, lon1, lat2, lon2) {
  const p = Math.PI / 180; // Math.PI / 180
  const c = Math.cos;
  const a =
    0.5 -
    c((lat2 - lat1) * p) / 2 +
    (c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p))) / 2;

  return Math.round(12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
}

/**
 * PUT /api/user/distance
 * Update distance unit for the user
 */
exports.updateDistance = (req, res) => {
  const { km } = req.body;
  User.findByIdAndUpdate(req.session.passport.user, {
    $set: {
      'profile.km': km,
    },
  }).then(user => {
    if (!user) {
      req.flash('errors', {
        msg: 'Account with that id does not exist.',
      });
    }
    res.send(user);
  });
};

/**
 * PUT /api/user
 * Update a connected user by its id.
 */
exports.updateAnUser = (req, res) => {
  const {
    name,
    location,
    description,
    skills,
    picture,
    activity,
    scope,
    contact,
    availability,
    conditions,
    link,
    latlng,
  } = req.body;
  if (picture) {
    // TODO : resize picture
    console.log(typeof picture);
    sharp(
      Buffer.from(picture.replace(/^data:image\/[a-z]+;base64,/, ''), 'base64'),
    )
      .resize(400, 400, {
        fit: 'inside',
      })
      .toBuffer()
      .then(data => {
        const photo = `data:image/jpeg;base64,${data.toString('base64')}`;
        console.log(photo);
        User.findByIdAndUpdate(req.session.passport.user, {
          $set: {
            'profile.picture': photo,
            'profile.name': name,
            'profile.location': location,
            'profile.activity': activity,
            'profile.latlng': latlng,
          },
        }).then(user => {
          console.log(user.email);
          if (!user) {
            req.flash('errors', {
              msg: 'Account with that id does not exist.',
            });
          }
          res.send(user);
        });
      })
      .catch(err => console.log(`downsize issue ${err}`));
  } else {
    User.findByIdAndUpdate(req.session.passport.user, {
      $set: {
        'profile.name': name,
        'profile.location': location,
        'profile.latlng': latlng,
        'profile.description': description,
        'profile.skills': skills,
        'profile.activity': activity,
        'profile.scope': scope,
        'profile.contact': contact,
        'profile.availability': availability,
        'profile.conditions': conditions,
        'profile.link': link,
      },
    }).then(user => {
      console.log(user.email);
      if (!user) {
        req.flash('errors', {
          msg: 'Account with that id does not exist.',
        });
      }
      res.send(user);
    });
  }
};

/**
 * PUT /api/user/favorites
 * Add a favorite to a connected user by its id.
 */
exports.pushFavorite = (req, res) => {
  req.assert('favorites', 'Favorites cannot be blank').notEmpty();
  const errors = req.validationErrors();
  console.log(errors);
  if (errors) {
    req.flash('errors', errors);
  }
  const { favorites } = req.body;

  User.findByIdAndUpdate(req.session.passport.user, {
    $push: {
      'profile.favorites': favorites,
    },
  }).then(user => {
    if (!user) {
      req.flash('errors', {
        msg: 'Account with that id does not exist.',
      });
    }
    res.send(user);
  });
};

/**
 * DELETE /api/user/favorites
 * Delete a favorite to a connected user by its id.
 */
exports.delFavorite = (req, res) => {
  const { id } = req.query;

  User.findByIdAndUpdate(
    req.session.passport.user,
    { $pull: { 'profile.favorites': id } },
    { new: true },
    (err, user) => {
      if (err) {
        console.log(`ERROR: ${err}`);
      }
      res.send(user.profile.favorites);
    },
  );
};

/**
 * GET /api/user/favorites
 * Get the connected user by its id.
 */
exports.getFavorites = (req, res, next) => {
  if (req.isAuthenticated()) {
    User.findById(req.session.passport.user).then(user => {
      if (!user) {
        req.flash('errors', {
          msg: 'Account with that id does not exist.',
        });
      }
      const newArray = [];
      _.forEach(user.profile.favorites, (value, key) => {
        if (value) {
          newArray.push(new ObjectId(value));
        }
      });
      User.find({ _id: { $in: newArray } }).then(favorites => {
        if (user.profile && user.profile.latlng[0] && user.profile.latlng[1]) {
          const usersWithDistance = _.map(favorites, favorite => {
            if (
              favorite.profile &&
              favorite.profile.latlng[0] &&
              favorite.profile.latlng[1]
            ) {
              const kmTo = distance(
                user.profile.latlng[0],
                user.profile.latlng[1],
                favorite.profile.latlng[0],
                favorite.profile.latlng[1],
              );
              const userWithDistance = JSON.parse(JSON.stringify(favorite));
              userWithDistance.profile.distance = kmTo;
              return userWithDistance;
            }
            return favorite;
          });
          const sortedByDistance = _.sortBy(usersWithDistance, [
            'profile.distance',
          ]);
          res.send(sortedByDistance);
        }
      });
    });
  } else {
    req.flash('errors', {
      msg: 'User  not connected',
    });
  }
};

/**
 * GET /api/user
 * Get the connected user by its id.
 */
exports.getAnUser = (req, res, next) => {
  if (req.isAuthenticated()) {
    User.findById(req.session.passport.user).then(user => {
      if (!user) {
        req.flash('errors', {
          msg: 'Account with that id does not exist.',
        });
      }
      res.send(user);
    });
  } else {
    req.flash('errors', {
      msg: 'User  not connected',
    });
    next();
  }
};

/**
 * PUT /api/user/disable
 * Disable the connected user
 */
exports.disableUser = (req, res, next) => {
  User.findByIdAndUpdate(req.session.passport.user, {
    $set: { disabled: true },
  }).then(user => {
    if (!user) {
      req.flash('errors', {
        msg: 'Account with that id does not exist.',
      });
    }
    req.logout();
    req.flash('info', { msg: 'Your account has been disabled.' });
    res.send();
  });
};
