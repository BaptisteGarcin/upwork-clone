const User = require('../models/User');
const Conversation = require('../models/Conversation');
const Message = require('../models/Message');
const mongoose = require('mongoose');
const { ObjectId } = mongoose.Types;

/**
 * GET /conversations
 * Get all conversations.
 */
exports.getAllConversations = (req, res) => {
  if (req.isAuthenticated()) {
    const conversationsFound = [];
    Conversation.find()
      .populate([
        {
          path: 'participants',
          // Explicitly exclude `_id`
          select: 'vaqqerid profile.picture profile.name _id',
        },
        {
          path: 'messages.from',
          // Explicitly exclude `_id`
          select: 'vaqqerid profile.picture profile.name _id',
        },
      ])
      .then(conversations => {
        // res.send(conversations);
        conversations.map(conversation => {
          if (
            conversation.participants.find(
              participant => participant._id == req.session.passport.user,
            )
          ) {
            conversationsFound.push(conversation);
          }
        });
      })
      .then(() => res.send(conversationsFound));
  }
};

/**
 * POST /conversation
 * Create a conversation
 */
exports.postConversation = (req, res) => {
  if (req.isAuthenticated()) {
    User.findOne(
      { vaqqerid: req.body.participantId },
      '_id',
      (err, participant) => {
        Conversation.find(
          {
            participants: [req.session.passport.user, participant._id],
          },
          (error, convs) => {
            if (convs.length > 0) {
              console.log('conv already exists');
              console.log(convs);
              res.end();
            } else {
              Conversation.create({}, (error, newConv) => {
                newConv.participants.push(req.session.passport.user);
                newConv.participants.push(participant._id);
                newConv.save();
              });
              res.end();
            }
          },
        );
      },
    );
  }
};

/**
 * POST /message
 * Add a message to an existing conversation
 */
exports.postMessage = (req, res) => {
  if (req.body.conversationId) {
    Conversation.findById(req.body.conversationId).then(conversation => {
      conversation.messages.push({
        from: req.session.passport.user,
        text: req.body.text,
      });
      conversation.save();
      Conversation.populate(
        conversation,
        {
          path: 'messages.from',
          // Explicitly exclude `_id`
          select: 'vaqqerid profile.picture profile.name _id',
        },
        () => res.send(conversation),
      );
    });
  } else {
    this.postConversation(req, res);
  }
};
