/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import SignupPage from 'containers/SignupPage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import ForgotPasswordPage from '../ForgotPasswordPage';
import ContactPage from '../ContactPage';
import SearchPage from '../SearchPage';
import AfterSignupPage from '../AfterSignup';
import ProfilePage from '../ProfilePage';
import Profile from '../../components/Profile';
import FavoritesPage from '../FavoritesPage';
import SettingsPage from '../SettingsPage';
import AboutPage from '../AboutPage';
import TermsPage from '../TermsPage';
import GuidelinesPage from '../GuidelinesPage';
import HelpusPage from '../HelpusPage';
import ConversationsPage from '../ConversationsPage';

const rightItems = [
  {
    to: '/',
    content: <FormattedMessage id="app.components.Navbar.home" />,
    key: 'home',
    as: Link,
  },
  {
    to: '/profile',
    content: <FormattedMessage id="app.components.Navbar.profile" />,
    key: 'profile',
    as: Link,
  },
  {
    to: '/favorites',
    content: <FormattedMessage id="app.components.Navbar.favorites" />,
    key: 'fav',
    as: Link,
  },
  {
    to: '/search',
    content: <FormattedMessage id="app.components.Navbar.search" />,
    key: 'search',
    as: Link,
  },
];
const leftItems = [
  {
    to: '/logging',
    content: <FormattedMessage id="app.components.Navbar.login" />,
    key: 'login',
  },
  {
    to: '/register',
    content: <FormattedMessage id="app.components.Navbar.signup" />,
    key: 'register',
  },
];

const sidebarItems = [
  {
    to: '/about',
    key: 'about',
    content: <FormattedMessage id="app.components.Navbar.about" />,
    as: Link,
  },
  {
    to: '/terms',
    key: 'terms',
    content: <FormattedMessage id="app.components.Navbar.terms" />,
    as: Link,
  },
  {
    to: '/contact',
    key: 'contact',
    content: <FormattedMessage id="app.components.Navbar.contact" />,
    as: Link,
  },
  {
    to: '/helpus',
    key: 'helpus',
    content: <FormattedMessage id="app.components.Navbar.helpus" />,
    as: Link,
  },
  {
    to: '/guidelines',
    key: 'guidelines',
    content: <FormattedMessage id="app.components.Navbar.guidelines" />,
    as: Link,
  },
];

export default function App() {
  return (
    <div>
      <NavBar
        leftItems={leftItems}
        rightItems={rightItems}
        sidebarItems={sidebarItems}
      >
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/register" component={SignupPage} />
          <Route path="/logging" component={LoginPage} />
          <Route path="/forgotPass" component={ForgotPasswordPage} />
          <Route path="/contact" component={ContactPage} />
          <Route path="/search" component={SearchPage} />
          <Route path="/afterSignup" component={AfterSignupPage} />
          <Route path="/profile" component={ProfilePage} />
          <Route path="/profile/:name" render={Profile} />
          <Route path="/favorites" component={FavoritesPage} />
          <Route path="/settings" component={SettingsPage} />
          <Route path="/about" component={AboutPage} />
          <Route path="/terms" component={TermsPage} />
          <Route path="/helpus" component={HelpusPage} />
          <Route path="/guidelines" component={GuidelinesPage} />
          <Route path="/conversations" component={ConversationsPage} />
          <Route component={NotFoundPage} />
        </Switch>
      </NavBar>
    </div>
  );
}
