import _ from 'lodash';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LocaleToggle from 'containers/LocaleToggle';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import Footer from './Footer';

import {
  Icon,
  Image,
  Menu,
  Sidebar,
  Responsive,
  Dropdown,
  Header,
  Button,
  Segment,
  Divider,
} from 'semantic-ui-react';

const options = [
  {
    to: '/profile',
    key: 'user',
    text: <FormattedMessage id="app.components.Navbar.profile" />,
    icon: 'user',
    value: 'Profile',
    as: Link,
  },
  {
    to: '/favorites',
    key: 'favorites',
    text: <FormattedMessage id="app.components.Navbar.favorites" />,
    icon: 'star',
    value: 'Favorites',
    as: Link,
  },
  {
    to: '/settings',
    key: 'settings',
    text: <FormattedMessage id="app.components.Navbar.settings" />,
    icon: 'settings',
    value: 'Settings',
    as: Link,
  },
  {
    to: '/',
    key: 'sign-out',
    text: <FormattedMessage id="app.components.Navbar.signout" />,
    icon: 'sign out',
    value: 'logout',
    as: Link,
  },
];

const NavBarMobile = ({
  children,
  leftItems,
  sidebarItems,
  onPusherClick,
  onToggle,
  rightItems,
  visible,
  picture,
  height,
  name,
  vaqqerid,
}) => (
  <div>
    <Sidebar.Pushable>
      <Sidebar
        as={Menu}
        animation="overlay"
        icon="labeled"
        direction="right"
        items={rightItems}
        vertical
        visible={visible}
        onClick={onPusherClick}
        width="thin"
      >
        {vaqqerid ? (
          <div>
            <Menu.Item>
              <p style={{ color: 'teal' }}>{vaqqerid}</p>
            </Menu.Item>
          </div>
        ) : (
          <div>
            <br />
            <br />
            <Segment vertical>
              <Link to="/register">
                <Button color="yellow" onClick={onPusherClick}>
                  <span style={{ color: 'red' }}>
                    <FormattedMessage id="app.components.Navbar.signup" />
                  </span>
                </Button>
              </Link>
              <Divider section horizontal>
                <FormattedMessage id="Or" />
              </Divider>
              <Link to="/logging">
                <Button color="green" onClick={onPusherClick}>
                  <FormattedMessage id="Connect" />
                </Button>
              </Link>
            </Segment>
            <br />
          </div>
        )}
        {_.map(rightItems, item => (
          <Menu.Item {...item} to={item.to} as={Link} onClick={onPusherClick} />
        ))}
        {vaqqerid ? (
          <Menu.Item
            to="/"
            as={Link}
            content="Log out"
            onClick={() => {
              onPusherClick();
              axios.get('/logout').then(() => window.location.reload());
            }}
          />
        ) : (
          ''
        )}
        {_.map(sidebarItems, item => (
          <Menu.Item {...item} to={item.to} as={Link} onClick={onPusherClick} />
        ))}
      </Sidebar>
      <Sidebar.Pusher
        dimmed={visible}
        onClick={onPusherClick}
        style={{ display: 'flex', minHeight: '100vh', flexDirection: 'column' }}
      >
        <div style={{ flex: 1 }}>
          <Menu fixed="top">
            <Menu.Item>
              <Header to="/" as={Link} color="teal">
                Vaqqer
              </Header>
            </Menu.Item>

            <Menu.Menu position="left">
              {picture ? (
                <Menu.Item>
                  <Dropdown
                    trigger={<Image size="mini" circular src={picture} />}
                    pointing="top right"
                    floating
                    icon={null}
                    options={options}
                    defaultValue="account"
                    onChange={(event, data) => {
                      if (data.value === 'logout')
                        axios
                          .get('/logout')
                          .then(() => window.location.reload());
                    }}
                  />
                  <span>
                    &nbsp;&nbsp;{name}&nbsp;{vaqqerid}
                  </span>
                </Menu.Item>
              ) : (
                _.map(leftItems, item => (
                  <Menu.Item {...item} to={item.to} as={Link} />
                ))
              )}
            </Menu.Menu>
            <Menu.Item onClick={onToggle}>
              <Icon name="sidebar" color="red" />
            </Menu.Item>
          </Menu>
          {children}
        </div>
        <Footer />
      </Sidebar.Pusher>
    </Sidebar.Pushable>
  </div>
);

const NavBarDesktop = ({
  leftItems,
  rightItems,
  sidebarItems,
  picture,
  onPusherClick,
  visible,
  onToggle,
  height,
  children,
  name,
  vaqqerid,
}) => (
  <div>
    <Sidebar.Pushable>
      <Sidebar
        as={Menu}
        animation="overlay"
        icon="labeled"
        direction="right"
        vertical
        visible={visible}
        width="thin"
      >
        {vaqqerid ? (
          <div>
            <Menu.Item>
              <p style={{ color: 'teal' }}>{vaqqerid}</p>
            </Menu.Item>
          </div>
        ) : (
          <div>
            <br />
            <br />
            <Segment vertical>
              <Link to="/register">
                <Button color="yellow" onClick={onPusherClick}>
                  <span style={{ color: 'red' }}>
                    <FormattedMessage id="app.components.Navbar.signup" />
                  </span>
                </Button>
              </Link>
              <Divider section horizontal>
                <FormattedMessage id="Or" />
              </Divider>
              <Link to="/logging">
                <Button color="green" onClick={onPusherClick}>
                  <FormattedMessage id="Connect" />
                </Button>
              </Link>
            </Segment>
            <br />
          </div>
        )}
        {_.map(rightItems, item => (
          <Menu.Item {...item} to={item.to} as={Link} onClick={onPusherClick} />
        ))}
        {vaqqerid ? (
          <Menu.Item
            to="/"
            as={Link}
            content="Log out"
            onClick={() => {
              onPusherClick();
              axios.get('/logout').then(() => window.location.reload());
            }}
          />
        ) : (
          ''
        )}
        {_.map(sidebarItems, item => (
          <Menu.Item {...item} to={item.to} as={Link} onClick={onPusherClick} />
        ))}
      </Sidebar>

      <Sidebar.Pusher
        dimmed={visible}
        onClick={onPusherClick}
        style={{ display: 'flex', minHeight: '100vh', flexDirection: 'column' }}
      >
        <div style={{ flex: 1 }}>
          <Menu fixed="top">
            <Menu.Item>
              <Header to="/" as={Link} color="teal">
                Vaqqer
              </Header>
            </Menu.Item>
            <Menu.Menu position="left">
              {picture ? (
                <Menu.Item>
                  <Dropdown
                    trigger={<Image size="mini" circular src={picture} />}
                    pointing="top right"
                    floating
                    icon={null}
                    options={options}
                    defaultValue="account"
                    onChange={(event, data) => {
                      if (data.value === 'logout')
                        axios
                          .get('/logout')
                          .then(() => window.location.reload());
                    }}
                  />
                  <span>
                    &nbsp;&nbsp;{name}&nbsp;{vaqqerid}
                  </span>
                </Menu.Item>
              ) : (
                _.map(leftItems, item => (
                  <Menu.Item {...item} to={item.to} as={Link} />
                ))
              )}
            </Menu.Menu>
            <Menu.Menu position="right">
              {_.map(rightItems, item => (
                <Menu.Item {...item} to={item.to} as={Link} />
              ))}
              <LocaleToggle />
            </Menu.Menu>
            <Menu.Item onClick={onToggle}>
              <Icon name="sidebar" color="red" />
            </Menu.Item>
          </Menu>
          {children}
        </div>
        <Footer />
      </Sidebar.Pusher>
    </Sidebar.Pushable>
  </div>
);

const NavBarChildren = ({ children, vaqqerid }) => (
  <div
    style={{
      marginTop: '4.3rem',
    }}
  >
    {vaqqerid ? (
      ''
    ) : (
      <Header style={{ paddingTop: '4.3rem' }} textAlign="center" size="medium">
        <FormattedMessage id="app.components.Footer.content" />
        <Link to="/register">
          <Button color="yellow">
            <span style={{ color: 'red' }}>
              <FormattedMessage id="app.components.Navbar.signup" />
            </span>
          </Button>
        </Link>
        <Link to="/logging">
          <Button color="green">
            <FormattedMessage id="Connect" />
          </Button>
        </Link>
      </Header>
    )}
    {children}
  </div>
);
class NavBar extends Component {
  state = {
    visible: false,
    name: '',
    vaqqerid: '',
    picture: '',
    height: '0vh',
  };

  componentDidMount = () => {
    axios
      .get('/api/user')
      .then(response => {
        if (response.status === 200) {
          const picture =
            response.data.profile && response.data.profile.picture
              ? response.data.profile.picture
              : 'https://i2.wp.com/www.ahfirstaid.org/wp-content/uploads/2014/07/avatar-placeholder.png';
          this.setState({
            picture,
            name: response.data.profile.name,
            vaqqerid: response.data.vaqqerid,
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  handlePusher = () => {
    const { visible } = this.state;
    if (visible) this.setState({ visible: false, height: '0vh' });
  };

  handleToggle = () =>
    this.setState({ visible: !this.state.visible, height: '100vh' });

  render() {
    const { children, leftItems, rightItems, sidebarItems } = this.props;
    const { visible } = this.state;

    return (
      <div>
        <Responsive {...Responsive.onlyMobile}>
          <NavBarMobile
            leftItems={leftItems}
            onPusherClick={this.handlePusher}
            onToggle={this.handleToggle}
            rightItems={rightItems}
            visible={visible}
            picture={this.state.picture}
            name={this.state.name}
            vaqqerid={this.state.vaqqerid}
            height={this.state.height}
            sidebarItems={sidebarItems}
          >
            <NavBarChildren vaqqerid={this.state.vaqqerid}>
              {children}
            </NavBarChildren>
          </NavBarMobile>
        </Responsive>
        <Responsive minWidth={Responsive.onlyTablet.minWidth}>
          <NavBarDesktop
            leftItems={leftItems}
            onPusherClick={this.handlePusher}
            onToggle={this.handleToggle}
            rightItems={rightItems}
            visible={visible}
            picture={this.state.picture}
            name={this.state.name}
            vaqqerid={this.state.vaqqerid}
            height={this.state.height}
            sidebarItems={sidebarItems}
          >
            <NavBarChildren vaqqerid={this.state.vaqqerid}>
              {children}
            </NavBarChildren>
          </NavBarDesktop>
        </Responsive>
      </div>
    );
  }
}

export default NavBar;
