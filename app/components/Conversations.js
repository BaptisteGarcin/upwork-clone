import React, { Component } from 'react';
import axios from 'axios';
import { push } from 'react-router-redux';
import faker from 'faker';
import {
  Form,
  TextArea,
  Grid,
  Divider,
  Header,
  Segment,
  Icon,
  Image,
  Comment,
  Dropdown,
  Menu,
  Label,
  Input,
  Item,
  Container,
  Placeholder,
  Card,
} from 'semantic-ui-react';
import avatarPlaceholder from '../images/avatar-placeholder.png';

const fakeParticipant1 = {
  id: 1,
  name: faker.name.findName(),
  image: faker.internet.avatar(),
};

const fakeParticipant2 = {
  id: 2,
  name: faker.name.findName(),
  image: faker.internet.avatar(),
};

const fakeParticipant3 = {
  id: 3,
  name: faker.name.findName(),
  image: faker.internet.avatar(),
};

const conv1 = {
  id: 1,
  participants: [fakeParticipant1, fakeParticipant2],
  messages: [
    {
      from: fakeParticipant1,
      date: faker.date.past(),
      text: faker.lorem.sentence(),
    },
    {
      from: fakeParticipant2,
      date: faker.date.past(),
      text: faker.lorem.sentence(),
    },
  ],
};

const conv2 = {
  id: 2,
  participants: [fakeParticipant2, fakeParticipant3],
  messages: [
    {
      from: fakeParticipant2,
      date: faker.date.past(),
      text: faker.lorem.sentence(),
    },
    {
      from: fakeParticipant3,
      date: faker.date.past(),
      text: faker.lorem.sentence(),
    },
  ],
};

const conv3 = {
  id: 2,
  participants: [fakeParticipant1, fakeParticipant3],
  messages: [
    {
      from: fakeParticipant1,
      date: faker.date.past(),
      text: faker.lorem.sentence(),
    },
    {
      from: fakeParticipant3,
      date: faker.date.past(),
      text: faker.lorem.sentence(),
    },
  ],
};
const convs = {
  conversations: [conv1, conv2, conv3],
};

const fakeUsers = Array.from({ length: 2 }, () => ({
  name: faker.name.findName(),
  sentence: faker.lorem.sentence(),
  description: faker.lorem.words(),
  image: faker.internet.avatar(),
  date: faker.date.past(),
}));

export default class Conversations extends Component {
  state = {
    convs,
    activeItem: 'inbox',
    newMessage: '',
    vaqqerid: '',
    conversations: [],
    conversationsList: [],
    activeConversation: null,
    loading: false,
  };

  componentDidMount = () => {
    axios.get('/api/user').then(response => {
      if (response.status === 200) {
        this.setState({
          vaqqerid: response.data.vaqqerid,
        });
      }
    });
    axios
      .get('/conversation')
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          this.setState({ conversations: response.data });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleItemClick = (index, conversation) => {
    this.setState({ activeItem: index, activeConversation: conversation });
  };

  handleSubmit = () => {
    axios
      .post('/message', {
        conversationId: this.state.activeConversation._id,
        text: this.state.newMessage,
      })
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          this.setState({ activeConversation: response.data, newMessage: '' });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    const { newMessage, conversations, activeItem, loading } = this.state;
    const { intl } = this.props;

    return (
      <Container>
        <Grid columns={2} divided celled>
          <Grid.Row>
            <Grid.Column
              width={4}
              verticalAlign="middle"
              style={{ minWidth: '115px' }}
            >
              <b>All conversations</b>
              <Dropdown inline />
            </Grid.Column>
            <Grid.Column width={12}>
              <Comment.Group size="large">
                <Comment>
                  <Comment.Avatar
                    as="a"
                    src="https://react.semantic-ui.com/images/avatar/small/joe.jpg"
                  />
                  <Comment.Content>
                    <Comment.Author as="a">Joe Henderson</Comment.Author>
                    <Comment.Metadata>
                      <span>5 days ago</span>
                    </Comment.Metadata>
                    <Comment.Text>
                      Dude, this is awesome. Thanks so much
                    </Comment.Text>
                  </Comment.Content>
                </Comment>
              </Comment.Group>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row
            style={{
              minHeight: '48.5vh',
            }}
          >
            <Grid.Column width={4} style={{ padding: '0', overflow: 'auto' }}>
              <Menu vertical fluid compact style={{ border: 'none' }}>
                {conversations.map((conversation, index) =>
                  conversation.participants.map(user => {
                    if (user.vaqqerid !== this.state.vaqqerid) {
                      return (
                        <Menu.Item
                          key={index}
                          active={activeItem === index}
                          onClick={(e, d) => {
                            console.log(conversation);
                            this.handleItemClick(index, conversation);
                          }}
                        >
                          <Label circular color="red">
                            51
                          </Label>
                          <Image avatar circular src={user.profile.picture} />
                          {user.profile.name}
                        </Menu.Item>
                      );
                    }
                  }),
                )}
              </Menu>
            </Grid.Column>
            <Grid.Column width={12}>
              <Grid.Row
                style={{
                  overflow: 'auto',
                  height: '80%',
                }}
              >
                {this.state.activeConversation &&
                  this.state.activeConversation.messages &&
                  this.state.activeConversation.messages.map((message, i) => (
                    <Comment.Group key={i}>
                      <Comment>
                        {message.from.profile.picture ? (
                          <Comment.Avatar src={message.from.profile.picture} />
                        ) : (
                          <Comment.Avatar src={avatarPlaceholder} />
                        )}
                        <Comment.Content>
                          <Comment.Author as="a">
                            {message.from.profile.name}
                          </Comment.Author>
                          <Comment.Metadata>
                            {message.createdAt.toString()}
                          </Comment.Metadata>
                          <Comment.Text>{message.text}</Comment.Text>
                        </Comment.Content>
                      </Comment>
                    </Comment.Group>
                  ))}
              </Grid.Row>
              <Grid.Row
                verticalAlign="bottom"
                style={{
                  height: '20%',
                }}
              >
                <Form size="large" onSubmit={() => this.handleSubmit()}>
                  <Input
                    autoFocus
                    style={{
                      maxHeight: '60px',
                      padding: '10px',
                    }}
                    fluid
                    action="Send"
                    placeholder="Type a message..."
                    value={newMessage}
                    onChange={(e, d) => this.setState({ newMessage: d.value })}
                  />
                </Form>
              </Grid.Row>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}
