import React, { Component } from 'react';
import {
  Grid,
  Icon,
  Segment,
  Item,
  Button,
  Rating,
  Header,
  Input,
} from 'semantic-ui-react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import axios from 'axios';
import { injectIntl, FormattedMessage } from 'react-intl';

class Favorites extends Component {
  state = {
    users: '',
    isLoading: false,
    km: true,
  };

  componentDidMount() {
    axios
      .get('/api/user')
      .then(response => {
        if (response.status === 200) {
          this.setState({
            km: response.data.profile.km,
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
    axios
      .get('/api/user/favorites')
      .then(response => {
        // handle success
        if (response.status === 200) {
          this.setState({ users: response.data });
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  resetComponent = () => this.setState({ isLoading: false, value: '' });

  handleSearchChange = value => {
    axios
      .get('/api/user/favorites')
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          this.setState({ users: response.data });
        }
      })
      .catch(error => {
        console.log(error);
      });
    this.setState({ isLoading: true, value });

    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent();

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i');
      const isMatch = result =>
        re.test(result.profile.name) || re.test(result.profile.activity);
      this.setState({
        isLoading: false,
        users: _.filter(this.state.users, isMatch),
      });
    }, 600);
  };

  render() {
    const { intl } = this.props;

    return (
      <div>
        {this.state.users ? (
          <Grid
            columns="equal"
            style={{ height: '100%', margin: '1rem' }}
            stackable
          >
            <Grid.Column width={1} />
            <Grid.Column width={14}>
              <Segment
                raised
                style={{ marginTop: '5rem', marginBottom: '5rem' }}
              >
                <Item.Group>
                  <Input
                    fluid
                    style={{ margin: '2rem' }}
                    loading={this.state.isLoading}
                    icon="search"
                    iconPosition="left"
                    placeholder={intl.formatMessage({
                      id: 'app.components.Favorites.search',
                    })}
                    onChange={(event, data) =>
                      this.handleSearchChange(data.value)
                    }
                  />
                </Item.Group>
                <Item.Group link divided>
                  {_.map(this.state.users, (user, index) => (
                    <Item key={index}>
                      <Item.Image
                        circular
                        size="tiny"
                        avatar
                        src={user.profile.picture}
                        onClick={() => {
                          this.props.dispatch(
                            push({
                              pathname: `/profile/${user.profile.name}`,
                              user,
                            }),
                          );
                        }}
                      />
                      <Item.Content>
                        <Item.Header
                          onClick={() => {
                            this.props.dispatch(
                              push({
                                pathname: `/profile/${user.profile.name}`,
                                user,
                              }),
                            );
                          }}
                        >
                          {user.profile.name}
                        </Item.Header>
                        <Item.Meta>
                          <Button compact circular floated="right" inverted>
                            <Rating
                              maxRating={1}
                              defaultRating={1}
                              rating={1}
                              icon="star"
                              size="massive"
                              onRate={() => {
                                axios.delete('/api/user/favorites', {
                                  params: { id: user._id },
                                });
                                this.setState(prevState => ({
                                  users: prevState.users.filter(
                                    u => u !== user,
                                  ),
                                }));
                              }}
                            />
                          </Button>
                        </Item.Meta>
                        <Item.Meta
                          onClick={(e, d) => {
                            console.log(e);
                            this.props.dispatch(
                              push({
                                pathname: `/profile/${user.profile.name}`,
                                user,
                              }),
                            );
                          }}
                        >
                          <Icon link name="map marker alternate" />
                          {user.profile.location}
                          {user.profile.distance > 0 &&
                            this.state.km && (
                              <small style={{ color: 'red' }}>
                                {' - '} {user.profile.distance} km
                              </small>
                            )}
                          {user.profile.distance > 0 &&
                            !this.state.km && (
                              <small style={{ color: 'red' }}>
                              {' - '} {user.profile.distance / 1.6} miles
                              </small>
                            )}
                        </Item.Meta>

                        <Item.Description
                          onClick={(e, d) => {
                            console.log(e);
                            this.props.dispatch(
                              push({
                                pathname: `/profile/${user.profile.name}`,
                                user,
                              }),
                            );
                          }}
                        >
                          {user.profile.activity}
                        </Item.Description>
                      </Item.Content>
                    </Item>
                  ))}
                </Item.Group>
              </Segment>
            </Grid.Column>
          </Grid>
        ) : (
          <Header
            textAlign="center"
            size="huge"
            style={{ marginBottom: '33rem', marginTop: '10rem' }}
          >
            <FormattedMessage id="app.components.Favorites.noFav" />
          </Header>
        )}
      </div>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default injectIntl(connect(mapDispatchToProps)(Favorites));
