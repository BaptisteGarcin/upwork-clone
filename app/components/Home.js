import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Redirect } from 'react-router';
import axios from 'axios';
import { Image } from 'semantic-ui-react';
import welcome from '../images/welcome.png';

class Home extends Component {
  state = { vaqqerid: null };

  componentDidMount = () => {
    axios
      .get('/api/user')
      .then(response => {
        if (response.status === 200) {
          this.setState({ vaqqerid: response.data.vaqqerid });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <div>
        {this.state.vaqqerid ? (
          <Redirect to="/search" />
        ) : (
          <Image fluid src={welcome} />
        )}
      </div>
    );
  }
}

export default Home;
