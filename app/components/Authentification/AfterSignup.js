import React, { Component } from 'react';
import {
  Form,
  Grid,
  Header,
  Message,
  Segment,
  Checkbox,
  Confirm,
  Image,
  Radio,
  Loader,
} from 'semantic-ui-react';
import axios from 'axios';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import { injectIntl, FormattedMessage, FormattedHTMLMessage } from 'react-intl';

class AfterSignup extends Component {
  state = {
    name: '',
    email: '',
    location: '',
    activity: '',
    checked: false,
    error: '',
    success: '',
    open: true,
    picture: '',
    locationLoading: false,
    zoom: '1',
    radioValue: '',
  };

  componentWillMount = () => {
    axios
      .get('/api/user')
      .then(response => {
        this.setState({
          email: response.data.email,
          name: response.data.profile.name,
          location: response.data.profile.location,
          activity: response.data.profile.activity,
          picture: response.data.profile.picture,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleSubmit = () => {
    if (!this.state.checked) {
      console.log('error');
      this.setState({ error: "You didn't agree to the terms" });
    } else {
      axios
        .put('/api/user', {
          name: this.state.name,
          location: this.state.location,
          activity: this.state.activity,
          picture: this.state.picture,
          latlng: this.state.latlng,
        })
        .then(response => {
          this.setState({ error: '', success: 'Profile updated' });
          console.log(`button${response}`);
          window.location.reload();
          this.props.dispatch(push('/profile'));
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  render() {
    const { intl } = this.props;
    const {
      email,
      name,
      location,
      activity,
      error,
      success,
      picture,
    } = this.state;
    return (
      <div className="login-form">
        <div>
          <Confirm
            open={this.state.open}
            content={
              <div style={{ padding: '5rem' }}>
                <FormattedHTMLMessage id="app.components.AfterSignup.content" />
              </div>
            }
            cancelButton={intl.formatMessage({
              id: 'app.components.AfterSignup.cancel',
            })}
            confirmButton={intl.formatMessage({
              id: 'app.components.AfterSignup.confirm',
            })}
            onCancel={() => this.setState({ open: false })}
            onConfirm={() => this.setState({ open: false })}
          />
        </div>
        <Grid
          centered
          textAlign="left"
          style={{ height: '100%', margin: '1rem' }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h1" color="teal" textAlign="center">
              <FormattedMessage id="app.components.AfterSignup.welcome" />
            </Header>
            <Header as="h3" color="teal" textAlign="center">
              <FormattedMessage id="app.components.AfterSignup.complete" />
            </Header>
            <Form size="large" onSubmit={this.handleSubmit}>
              <Segment raised>
                <Form.Input
                  name="name"
                  type="text"
                  required
                  value={name}
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder={intl.formatMessage({
                    id: 'app.components.AfterSignup.name',
                  })}
                  onChange={e => this.setState({ name: e.target.value })}
                />
                <Form.Input
                  name="activity"
                  type="text"
                  required
                  value={activity}
                  fluid
                  placeholder={intl.formatMessage({
                    id: 'app.components.AfterSignup.occupation',
                  })}
                  onChange={e => this.setState({ activity: e.target.value })}
                />
                <Message
                  icon="at"
                  header={intl.formatMessage({
                    id: 'app.components.AfterSignup.email',
                  })}
                  content={email}
                />
                {picture && <Image centered src={picture} />}
                <br />
                <Form.Input
                  type="file"
                  label="Upload Your Photo"
                  name="photo"
                  id="photo"
                  color="green"
                  placeholder="Upload image"
                  icon="upload"
                  iconPosition="left"
                  onChange={(e, data) => {
                    const reader = new FileReader();
                    reader.onload = event => {
                      this.setState({ picture: event.target.result });
                    };
                    reader.readAsDataURL(e.target.files[0]);
                  }}
                />

                <Form>
                  <Form.Field>
                    <FormattedMessage id="app.components.AfterSignup.locationType" />
                    : <b>{this.state.value}</b>
                  </Form.Field>
                  <Form.Field>
                    <Radio
                      label="Use current location"
                      name="radioGroup"
                      value="current"
                      checked={this.state.radioValue === 'current'}
                      onChange={(e, { value }) => {
                        this.setState({ radioValue: value, zoom: '13' });
                        if (navigator.geolocation) {
                          const myPromise = new Promise((resolve, reject) => {
                            this.setState({ locationLoading: true });
                            navigator.geolocation.getCurrentPosition(position =>
                              resolve(position),
                            );
                          });
                          myPromise.then(position =>
                            this.setState({
                              locationLoading: false,
                              latlng: [
                                position.coords.latitude,
                                position.coords.longitude,
                              ],
                            }),
                          );
                        }
                      }}
                    />
                    <Loader
                      size="mini"
                      active={this.state.locationLoading}
                      inline
                    />
                  </Form.Field>
                  <Form.Field>
                    <Radio
                      label="Use custom location"
                      name="radioGroup"
                      value="custom"
                      checked={this.state.radioValue === 'custom'}
                      onChange={(e, { value }) =>
                        this.setState({ radioValue: value })
                      }
                    />
                  </Form.Field>
                </Form>
                {this.state.radioValue && (
                  <Map
                    center={this.state.latlng}
                    zoom={this.state.zoom}
                    onClick={e => {
                      console.log(e);
                      this.setState({
                        latlng: [e.latlng.lat, e.latlng.lng],
                      });
                    }}
                  >
                    <Loader active={this.state.locationLoading} />
                    <TileLayer
                      attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                      url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={this.state.latlng}>
                      <Popup>
                        <FormattedMessage id="app.components.YourLocation" />
                      </Popup>
                    </Marker>
                  </Map>
                )}
                <br />
                <Form.Input
                  fluid
                  placeholder={intl.formatMessage({
                    id: 'app.components.AfterSignup.location',
                  })}
                  name="location"
                  type="text"
                  icon="map marker alternate"
                  iconPosition="left"
                  required
                  value={location}
                  onChange={e => this.setState({ location: e.target.value })}
                />
                <Segment style={{ overflow: 'auto', maxHeight: 200 }}>
                  <Message
                    header={intl.formatMessage({
                      id: 'app.components.AfterSignup.terms',
                    })}
                    content={
                      <FormattedHTMLMessage id="app.components.AfterSignup.termsContent" />
                    }
                  />
                </Segment>

                <Checkbox
                  label="I have read the Terms and Conditions"
                  onChange={(e, data) =>
                    this.setState({ checked: data.checked })
                  }
                />
                <Checkbox
                  label="I accept and agree with terms of contract"
                  onChange={(e, data) =>
                    this.setState({ checked: data.checked })
                  }
                />
                <br />
                <br />
                {error && (
                  <Message negative>
                    <Message.Header>
                      <FormattedMessage id="app.components.AfterSignup.error" />
                    </Message.Header>
                    <p>{error}</p>
                  </Message>
                )}
                {success && (
                  <Message positive>
                    <Message.Header>
                      <FormattedMessage id="app.components.AfterSignup.success" />
                    </Message.Header>
                    <p>{success}</p>
                  </Message>
                )}
                <Form.Button color="teal" fluid size="large">
                  <FormattedMessage id="app.components.AfterSignup.save" />
                </Form.Button>
              </Segment>
            </Form>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default injectIntl(connect(mapDispatchToProps)(AfterSignup));
