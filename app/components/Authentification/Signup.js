import React, { Component } from 'react';
import {
  Form,
  Grid,
  Header,
  Segment,
  Divider,
  Message,
} from 'semantic-ui-react';
import axios from 'axios';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Link } from 'react-router-dom';
import { injectIntl, FormattedMessage } from 'react-intl';
class Signup extends Component {
  state = {
    password: '',
    email: '',
    confirmPassword: '',
  };

  handleSubmit = () => {
    axios
      .post('/signup', {
        email: this.state.email,
        password: this.state.password,
        confirmPassword: this.state.confirmPassword,
      })
      .then(response => {
        this.props.dispatch(push('/afterSignup'));
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    const { password, email, confirmPassword } = this.state;
    const { intl } = this.props;

    return (
      <div className="signup-form">
        <Grid
          textAlign="center"
          style={{ height: '100%', margin: '1rem' }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header
              as="h2"
              color="teal"
              textAlign="center"
              style={{ marginTop: '3rem', marginBottom: '3rem' }}
            >
              Sign up a new account
            </Header>
            <Form size="large" onSubmit={this.handleSubmit}>
              <Segment>
                <Form.Input
                  name="email"
                  type="email"
                  autoFocus="autofocus"
                  autoComplete="email"
                  required
                  value={email}
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder={intl.formatMessage({
                    id: 'Email',
                  })}
                  onChange={e => this.setState({ email: e.target.value })}
                />
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder={intl.formatMessage({
                    id: 'Password',
                  })}
                  name="password"
                  type="password"
                  required
                  value={password}
                  onChange={e => this.setState({ password: e.target.value })}
                />
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder={intl.formatMessage({
                    id: 'ConfirmPassword',
                  })}
                  name="confirmPassword"
                  type="password"
                  required
                  value={confirmPassword}
                  onChange={e =>
                    this.setState({ confirmPassword: e.target.value })
                  }
                />
                <Form.Button color="teal" fluid size="large">
                  <FormattedMessage id="SignUp" />
                </Form.Button>
              </Segment>
            </Form>
            <Divider horizontal>Or</Divider>
            <Message style={{ marginBottom: '3rem' }}>
              <FormattedMessage id="AlreadyHaveAnAccount" />
              <Link to="/logging">
                <FormattedMessage id="Login" />
              </Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default injectIntl(connect(mapDispatchToProps)(Signup));
