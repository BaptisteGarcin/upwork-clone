import React, { Component } from 'react';
import { Grid, Button } from 'semantic-ui-react';
import { Redirect } from 'react-router';
import axios from 'axios';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Link } from 'react-router-dom';
import { injectIntl, FormattedMessage } from 'react-intl';
class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      km: true,
      email: '',
    };
  }

  componentDidMount = () => {
    axios
      .get('/api/user')
      .then(response => {
        if (response.status === 200) {
          this.setState({
            email: response.data.email,
            km: response.data.profile.km,
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <Grid
        columns="equal"
        style={{ height: '100%', margin: '1rem', marginBottom: '20rem' }}
        stackable
      >
        <Grid.Column width={1} />
        <Grid.Column width={14}>
          Show distance in :{' '}
          <Button
            toggle
            active={this.state.km}
            onClick={() =>
              this.setState({ km: !this.state.km }, () => {
                axios
                  .put('/api/user/distance', {
                    km: this.state.km,
                  })
                  .then(response => {
                    console.log(response);
                  });
              })
            }
          >
            Km
          </Button>
          <Button
            toggle
            active={!this.state.km}
            onClick={() => {
              this.setState({ km: !this.state.km }, () => {
                axios
                  .put('/api/user/distance', {
                    km: this.state.km,
                  })
                  .then(response => {
                    console.log('test');
                    console.log(response);
                  });
              });
            }}
          >
            Miles
          </Button>
          <br />
          <br />
          <Button
            color="grey"
            onClick={() =>
              axios
                .post('/forgot', {
                  email: this.state.email,
                })
                .then(response => {})
                .catch(error => {
                  console.log(error);
                })
            }
          >
            Ask for a password change
          </Button>
          <br />
          <br />
          <Button
            color="red"
            onClick={() =>
              axios.post('/account/delete').then(() => {
                this.props.dispatch(push('/'));
                window.location.reload();
              })
            }
          >
            Delete account
          </Button>
          <br />
          <br />
          <Button
            color="red"
            onClick={() =>
              axios.put('/api/user/disable').then(() => {
                this.props.dispatch(push('/'));
                window.location.reload();
              })
            }
          >
            Disable account
          </Button>
        </Grid.Column>
      </Grid>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default injectIntl(connect(mapDispatchToProps)(Settings));
