import _ from 'lodash';
import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import {
  Input,
  Grid,
  Segment,
  Icon,
  Divider,
  Item,
  Button,
  Rating,
  Header,
  Menu,
  Container,
  Form,
  Radio,
  Loader,
  Modal,
} from 'semantic-ui-react';
import axios from 'axios';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Contact from './Contact';

class Search extends Component {
  state = {
    isLoading: false,
    users: [],
    value: '',
    radioValue: '',
    category: 'members or activities',
    rating: 0,
    km: true,
    searchByLocation: '',
    activeItem: 'members or activities',
    toggleMap: false,
    latlng: ['', ''],
    zoom: '1',
    vaqqerid: '',
    locationLoading: false,
  };

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name, category: name });
  };

  resetComponent = () => this.setState({ isLoading: false, value: '' });

  componentDidMount = () => {
    axios
      .get('/api/user')
      .then(response => {
        if (response.status === 200) {
          this.setState({
            km: response.data.profile.km,
            vaqqerid: response.data.vaqqerid,
          });
          if (response.data.profile.latlng[0]) {
            this.setState(
              {
                searchByLocation: [
                  response.data.profile.latlng[0],
                  response.data.profile.latlng[1],
                ],
              },
              () => {
                axios
                  .get('/api/users', {
                    params: {
                      searchByLocation: this.state.searchByLocation,
                    },
                  })
                  .then(response => {
                    if (response.status === 200) {
                      console.log(response.data);
                      this.setState({ users: response.data });
                    }
                  })
                  .catch(error => {
                    console.log(error);
                  });
              },
            );
          }
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleSearchChange = value => {
    axios
      .get('/api/users', {
        params: {
          searchByLocation: this.state.searchByLocation,
        },
      })
      .then(response => {
        if (response.status === 200) {
          console.log(response.data);
          this.setState({ users: response.data });
        }
      })
      .catch(error => {
        console.log(error);
      });
    this.setState({ isLoading: true, value });

    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent();

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i');
      let isMatch;
      if (this.state.category === 'members or activities') {
        isMatch = result =>
          re.test(result.profile.name) || re.test(result.profile.activity);
      } else {
        isMatch = result => re.test(result.profile[this.state.category]);
      }
      this.setState({
        isLoading: false,
        users: _.filter(this.state.users, isMatch),
      });
    }, 600);
  };

  handleDropdown = aCategory => {
    this.setState({ category: aCategory });
  };

  render() {
    const { isLoading, activeItem, vaqqerid } = this.state;
    return (
      <div>
        {vaqqerid ? (
          <Grid
            columns="equal"
            style={{ height: '100%', margin: '1rem' }}
            stackable
          >
            <Grid.Column width={4}>
              <Menu
                vertical
                style={{
                  marginTop: '10rem',
                  marginBottom: '5rem',
                  paddingBottom: '4rem',
                  fontSize: '1.4em',
                  border: 'none',
                }}
              >
                <Menu.Item>
                  <Menu.Menu>
                    <Menu.Item
                      name="members or activities"
                      active={activeItem === 'members or activities'}
                      onClick={this.handleItemClick}
                    >
                      All
                    </Menu.Item>
                  </Menu.Menu>
                </Menu.Item>
                <Menu.Item>
                  <Menu.Menu>
                    <Menu.Item
                      name="name"
                      active={activeItem === 'name'}
                      onClick={this.handleItemClick}
                    >
                      Members
                    </Menu.Item>
                  </Menu.Menu>
                </Menu.Item>
                <Menu.Item>
                  <Menu.Menu>
                    <Menu.Item
                      name="activity"
                      active={activeItem === 'activity'}
                      onClick={this.handleItemClick}
                    >
                      Activities
                    </Menu.Item>
                  </Menu.Menu>
                </Menu.Item>
                <Menu.Item />
              </Menu>
            </Grid.Column>
            <Grid.Column width={8}>
              <Segment
                raised
                style={{ marginTop: '5rem', marginBottom: '5rem' }}
              >
                <Item.Group>
                  <Input
                    fluid
                    style={{ margin: '2rem' }}
                    loading={isLoading}
                    icon="search"
                    iconPosition="left"
                    placeholder={`Search ${this.state.category}...`}
                    onChange={(event, data) =>
                      this.handleSearchChange(data.value)
                    }
                  />
                </Item.Group>
                <Container
                  textAlign="right"
                  style={{ color: 'red' }}
                  onClick={() =>
                    this.setState({ toggleMap: !this.state.toggleMap })
                  }
                >
                  Switch position
                </Container>
                {this.state.toggleMap && (
                  <div>
                    <Form>
                      <Form.Field>
                        Position for search : <b>{this.state.value}</b>
                      </Form.Field>
                      <Form.Field>
                        <Radio
                          label="Use current location"
                          name="radioGroup"
                          value="current"
                          checked={this.state.radioValue === 'current'}
                          onChange={(e, { value }) => {
                            this.setState({ radioValue: value, zoom: '13' });
                            if (navigator.geolocation) {
                              const myPromise = new Promise(
                                (resolve, reject) => {
                                  this.setState({ locationLoading: true });
                                  navigator.geolocation.getCurrentPosition(
                                    position => resolve(position),
                                  );
                                },
                              );
                              myPromise.then(position =>
                                this.setState(
                                  {
                                    locationLoading: false,
                                    searchByLocation: [
                                      position.coords.latitude,
                                      position.coords.longitude,
                                    ],
                                    latlng: [
                                      position.coords.latitude,
                                      position.coords.longitude,
                                    ],
                                  },
                                  () => {
                                    this.handleSearchChange('');
                                  },
                                ),
                              );
                            }
                          }}
                        />
                        <Loader
                          size="mini"
                          active={this.state.locationLoading}
                          inline
                        />
                      </Form.Field>
                      <Form.Field>
                        <Radio
                          label="Use custom location"
                          name="radioGroup"
                          value="custom"
                          checked={this.state.radioValue === 'custom'}
                          onChange={(e, { value }) =>
                            this.setState({ radioValue: value })
                          }
                        />
                      </Form.Field>
                    </Form>
                    <Map
                      center={this.state.latlng}
                      zoom={this.state.zoom}
                      onClick={e => {
                        console.log(e);
                        this.setState(
                          {
                            searchByLocation: [e.latlng.lat, e.latlng.lng],
                            latlng: [e.latlng.lat, e.latlng.lng],
                          },
                          () => {
                            this.handleSearchChange('');
                          },
                        );
                      }}
                    >
                      <Loader active={this.state.locationLoading} />
                      <TileLayer
                        attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                        url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
                      />
                      {this.state.latlng[0] &&
                        this.state.latlng[1] && (
                        <Marker position={this.state.latlng}>
                          <Popup>
                              <span>
                                A pretty CSS3 popup. <br /> Easily customizable.
                              </span>
                            </Popup>
                          </Marker>
                        )}
                    </Map>
                  </div>
                )}
                <Divider />
                <Item.Group link divided>
                  {_.map(this.state.users, (user, index) => (
                    <Item
                      key={index}
                      onClick={e => {
                        console.log(user);
                        this.props.dispatch(
                          push({
                            pathname: `/profile/${user.profile.name}`,
                            user,
                          }),
                        );
                      }}
                    >
                      <Item.Image size="small" src={user.profile.picture} />
                      <Item.Content>
                        <Item.Meta>
                          <Button compact circular floated="right" inverted>
                            <Rating
                              maxRating={1}
                              defaultRating={0}
                              icon="star"
                              size="massive"
                              onRate={e => {
                                e.stopPropagation();

                                axios
                                  .put('/api/user/favorites', {
                                    favorites: user._id,
                                  })
                                  .then(response => {
                                    console.log(response);
                                  })
                                  .catch(error => {
                                    console.log(error);
                                  });
                              }}
                            />
                          </Button>
                        </Item.Meta>
                        <Item.Header>{user.profile.activity}</Item.Header>
                        <Item.Description>
                          {user.profile.name}
                          <Container textAlign="right">
                            {user.vaqqerid}
                          </Container>
                        </Item.Description>
                        <Item.Meta
                          style={{
                            marginTop: '4rem',
                            color: '#88DF88',
                            fontSize: '1.3em',
                          }}
                        >
                          <Icon
                            link
                            size="big"
                            inverted
                            color="red"
                            name="map marker alternate"
                          />
                          {user.profile.location}
                          {user.profile.distance &&
                            this.state.km && (
                              <small style={{ color: 'red' }}>
                              {' - '} {user.profile.distance} km
                            </small>
                            )}
                          {user.profile.distance &&
                            !this.state.km && (
                              <small style={{ color: 'red' }}>
                              {' - '} {user.profile.distance / 1.6} miles
                            </small>
                            )}

                          <Modal
                            onClick={e => {
                              e.stopPropagation();
                              e.preventDefault();
                            }}
                            trigger={
                              <Button
                                onClick={e => {
                                  e.stopPropagation();
                                  e.preventDefault();
                                }}
                                circular
                                floated="right"
                              >
                                Signal
                              </Button>
                            }
                            closeIcon
                          >
                            <Header icon="send" content="Report this user" />
                            <Modal.Content>
                              <Contact />
                            </Modal.Content>
                          </Modal>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                  ))}
                </Item.Group>
              </Segment>
            </Grid.Column>
            <Grid.Column width={4} />
          </Grid>
        ) : (
          <Header
            textAlign="center"
            size="huge"
            style={{ marginBottom: '33rem', marginTop: '10rem' }}
          >
            <FormattedMessage id="app.components.Search.notConnected" />
          </Header>
        )}
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(Search);
