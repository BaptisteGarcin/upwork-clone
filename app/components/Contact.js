import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import {
  Form,
  Input,
  TextArea,
  Button,
  Grid,
  Divider,
  Header,
  Segment,
  Icon,
} from 'semantic-ui-react';
import { injectIntl, FormattedMessage } from 'react-intl';

class Contact extends Component {
  state = {
    name: '',
    email: '',
    message: '',
    vaqqerid: '',
  };

  handleSubmit = () => {
    axios
      .post('/contact', {
        name: this.state.name,
        email: this.state.email,
        message: this.state.message,
        vaqqerid: this.state.vaqqerid,
      })
      .then(response => {
        this.props.dispatch(push('/logging'));
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    const { name, email, message, vaqqerid } = this.state;
    const { intl } = this.props;

    return (
      <Grid
        centered
        style={{ height: '100%', margin: '1rem' }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450, marginBottom: '3rem' }}>
          <Header
            as="h2"
            color="teal"
            textAlign="center"
            style={{ marginTop: '3rem', marginBottom: '3rem' }}
          >
            <FormattedMessage id="ContactUs" />
          </Header>
          <Form size="large" onSubmit={this.handleSubmit}>
            <Segment>
              <Form.Input
                name="name"
                type="text"
                autoFocus="autofocus"
                required
                value={name}
                fluid
                icon="user"
                iconPosition="left"
                placeholder="Name"
                onChange={e => this.setState({ name: e.target.value })}
              />
              <Form.Input
                name="vaqqerid"
                type="text"
                required
                value={vaqqerid}
                fluid
                icon="address card outline"
                iconPosition="left"
                placeholder="Optional: Your vaqqer Id"
                onChange={e => this.setState({ vaqqerid: e.target.value })}
              />
              <Form.Input
                name="email"
                type="email"
                autoComplete="email"
                required
                value={email}
                fluid
                icon="mail"
                iconPosition="left"
                placeholder={intl.formatMessage({
                  id: 'Email',
                })}
                onChange={e => this.setState({ email: e.target.value })}
              />
              <TextArea
                style={{ minHeight: 100 }}
                placeholder={intl.formatMessage({
                  id: 'app.components.Contact.desc',
                })}
                name="message"
                type="text"
                required
                value={message}
                onChange={e => this.setState({ message: e.target.value })}
              />
              <Divider />
              <Form.Button color="teal" fluid size="large">
                <FormattedMessage id="Send" />
                <Icon name="send" style={{ paddingLeft: '1em' }} />
              </Form.Button>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default injectIntl(connect(mapDispatchToProps)(Contact));
