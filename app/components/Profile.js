import React, { Component } from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import {
  Button,
  Form,
  Grid,
  Header,
  Icon,
  Item,
  Image,
  Menu,
  Rating,
  TextArea,
  Radio,
  Loader,
  Modal,
} from 'semantic-ui-react';
import axios from 'axios';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import LeafletMap from './Map';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      location: '',
      description: '',
      picture: '',
      activity: '',
      activeItem: 'scope',
      contact: '',
      availability: '',
      link: '',
      scope: '',
      conditions: '',
      vaqqerid: '',
      mode: 'read',
      latlng: ['', ''],
      distance: '',
      km: true,
      editable: false,
      locationLoading: false,
    };
  }

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name });
  };

  retrieveUserInfos = () => {};

  componentWillReceiveProps = () => {
    axios
      .get('/api/user')
      .then(response => {
        if (response.status === 200) {
          if (
            response.data.profile.latlng &&
            response.data.profile.latlng.length > 0
          ) {
            this.setState({
              latlng: response.data.profile.latlng,
            });
          }
          const picture =
            response.data.profile && response.data.profile.picture
              ? response.data.profile.picture
              : 'https://i2.wp.com/www.ahfirstaid.org/wp-content/uploads/2014/07/avatar-placeholder.png';
          this.setState({
            editable: true,
            mode: 'edit',
            name: response.data.profile.name,
            location: response.data.profile.name,
            description: response.data.profile.description,
            picture,
            km: response.data.profile.km,
            vaqqerid: response.data.vaqqerid,
            activity: response.data.profile.activity,
            scope: response.data.profile.scope,
            contact: response.data.profile.contact,
            availability: response.data.profile.availability,
            conditions: response.data.profile.conditions,
            link: response.data.profile.link,
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  componentDidMount = () => {
    if (this.props.location.get('user')) {
      this.setState({
        name: this.props.location
          .get('user')
          .get('profile')
          .get('name'),
        picture: this.props.location
          .get('user')
          .get('profile')
          .get('picture'),
        description: this.props.location
          .get('user')
          .get('profile')
          .get('description'),
        location: this.props.location
          .get('user')
          .get('profile')
          .get('location'),
        activity: this.props.location
          .get('user')
          .get('profile')
          .get('activity'),
        distance: this.props.location
          .get('user')
          .get('profile')
          .get('distance'),
        vaqqerid: this.props.location.get('user').get('vaqqerid'),
        latlng: this.props.location
          .get('user')
          .get('profile')
          .get('latlng')._tail.array,
      });
    } else {
      axios
        .get('/api/user')
        .then(response => {
          if (response.status === 200) {
            if (
              response.data.profile.latlng &&
              response.data.profile.latlng.length > 0
            ) {
              this.setState({
                latlng: response.data.profile.latlng,
              });
            }
            const picture =
              response.data.profile && response.data.profile.picture
                ? response.data.profile.picture
                : 'https://i2.wp.com/www.ahfirstaid.org/wp-content/uploads/2014/07/avatar-placeholder.png';
            this.setState({
              editable: true,
              name: response.data.profile.name,
              location: response.data.profile.location,
              description: response.data.profile.description,
              picture,
              vaqqerid: response.data.vaqqerid,
              activity: response.data.profile.activity,
              scope: response.data.profile.scope,
              contact: response.data.profile.contact,
              availability: response.data.profile.availability,
              conditions: response.data.profile.conditions,
              link: response.data.profile.link,
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  handleSave = () => {
    this.setState({ mode: 'read' });
    const {
      name,
      location,
      description,
      activity,
      scope,
      contact,
      latlng,
      availability,
      conditions,
      link,
    } = this.state;
    axios
      .put('/api/user', {
        name,
        location,
        description,
        latlng,
        activity,
        scope,
        contact,
        availability,
        conditions,
        link,
      })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    const {
      name,
      location,
      picture,
      activeItem,
      activity,
      scope,
      conditions,
      contact,
      availability,
      link,
      vaqqerid,
      distance,
    } = this.state;
    const { intl } = this.props;
    return (
      <div>
        {vaqqerid ? (
          <Grid stackable centered style={{ paddingTop: '5rem' }}>
            {!this.state.editable && (
              <Button
                color="green"
                onClick={() => {
                  axios
                    .post('/conversation', {
                      participantId: this.state.vaqqerid,
                    })
                    .then(response => {
                      // TODO: Send profile id so that we can know which conversation to focus
                      this.props.dispatch(push('/conversations'));
                    })
                    .catch(error => {
                      console.log(error);
                    });
                }}
              >
                Send a message
                <Icon name="send" style={{ paddingLeft: '1em' }} />
              </Button>
            )}
            <Grid.Row>
              <Grid.Column width={3} textAlign="center">
                <Image
                  fluid
                  onClick={() => console.log('clicked')}
                  src={picture}
                />
                <br />
                {this.state.editable && (
                  <Form>
                    <Form.Input
                      type="file"
                      label="Edit Your Photo"
                      name="photo"
                      id="photo"
                      color="green"
                      placeholder={intl.formatMessage({
                        id: 'UploadImage',
                      })}
                      icon="upload"
                      iconPosition="left"
                      onChange={(e, data) => {
                        const reader = new FileReader();
                        reader.onload = event => {
                          this.setState(
                            {
                              picture: event.target.result,
                            },
                            () => {
                              axios
                                .put('/api/user', {
                                  picture: this.state.picture,
                                  name: this.state.name,
                                  location: this.state.location,
                                  activity: this.state.activity,
                                  latlng: this.state.latlng,
                                })
                                .then(() => window.location.reload());
                            },
                          );
                        };
                        reader.readAsDataURL(e.target.files[0]);
                      }}
                    />
                  </Form>
                )}
              </Grid.Column>
              <Grid.Column verticalAlign="middle" textAlign="left" width={9}>
                <Item.Group link divided>
                  <Item>
                    <Item.Content>
                      <Item.Meta>
                        <Button compact circular floated="right" inverted>
                          <Rating
                            maxRating={1}
                            defaultRating={0}
                            icon="star"
                            size="massive"
                            onRate={() => {
                              axios
                                .put('/api/user/favorites', {
                                  favorites: user._id,
                                })
                                .then(response => {
                                  console.log(response);
                                })
                                .catch(error => {
                                  console.log(error);
                                });
                            }}
                          />
                        </Button>
                      </Item.Meta>
                      {this.state.mode === 'edit' ? (
                        <TextArea
                          style={{ border: '2px solid black' }}
                          autoHeight
                          name="activity"
                          type="text"
                          placeholder={intl.formatMessage({
                            id: 'app.components.Profile.activity',
                          })}
                          autoFocus
                          value={activity}
                          onChange={e =>
                            this.setState({ activity: e.target.value })
                          }
                        />
                      ) : (
                        <Item.Header>{activity}</Item.Header>
                      )}
                      <br />

                      {this.state.mode === 'edit' ? (
                        <TextArea
                          style={{ border: '2px solid black' }}
                          autoHeight
                          name="name"
                          type="text"
                          placeholder={intl.formatMessage({
                            id: 'app.components.Profile.activity',
                          })}
                          autoFocus
                          value={name}
                          onChange={e =>
                            this.setState({ name: e.target.value })
                          }
                        />
                      ) : (
                        <Item.Description>{name}</Item.Description>
                      )}
                      <Button
                        compact
                        circular
                        floated="right"
                        style={{ backgroundColor: 'white' }}
                      >
                        {vaqqerid}
                      </Button>
                      <Item.Meta
                        style={{
                          marginTop: '4rem',
                          color: '#88DF88',
                          fontSize: '1.3em',
                        }}
                      >
                        <Icon
                          link
                          size="big"
                          inverted
                          color="red"
                          name="map marker alternate"
                        />
                        {location}
                        {distance &&
                          this.state.km && (
                            <small style={{ color: 'red' }}>
                            {' - '} {distance} km
                          </small>
                        )}
                        {distance &&
                          !this.state.km && (
                            <small style={{ color: 'red' }}>
                            {' - '} {distance / 1.6} miles
                          </small>
                          )}
                        {this.state.editable && (
                          <Button
                            circular
                            floated="right"
                            onClick={() => this.setState({ editable: false })}
                          >
                            <FormattedMessage id="app.components.Profile.publicView" />
                          </Button>
                        )}
                        {!this.state.editable && (
                          <Modal
                            onClick={e => {
                              e.stopPropagation();
                              e.preventDefault();
                            }}
                            trigger={
                              <Button
                                circular
                                floated="right"
                                onClick={e => {
                                  e.stopPropagation();
                                  e.preventDefault();
                                }}
                              >
                                <FormattedMessage id="app.components.Profile.signal" />
                              </Button>
                            }
                            closeIcon
                          >
                            <Header
                              icon="archive"
                              content="Archive Old Messages"
                            />
                            <Modal.Content>
                              <p>
                                Your inbox is getting full, would you like us to
                                enable automatic archiving of old messages?
                              </p>
                            </Modal.Content>
                            <Modal.Actions>
                              <Button color="red">
                                <Icon name="remove" /> No
                              </Button>
                              <Button color="green">
                                <Icon name="checkmark" /> Yes
                              </Button>
                            </Modal.Actions>
                          </Modal>
                        )}
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                </Item.Group>
              </Grid.Column>
              <Grid.Column width={2} />
            </Grid.Row>

            <Grid.Row>
              <Grid.Column width={3}>
                <Menu
                  vertical
                  compact
                  style={{
                    marginTop: '2rem',
                    marginBottom: '5rem',
                    paddingBottom: '4rem',
                    fontSize: '1.4em',
                  }}
                >
                  <Menu.Item>
                    <Menu.Menu>
                      <Menu.Item
                        name="scope"
                        active={activeItem === 'scope'}
                        onClick={this.handleItemClick}
                      >
                        <FormattedMessage id="app.components.Profile.scope" />
                      </Menu.Item>
                    </Menu.Menu>
                  </Menu.Item>
                  <Menu.Item>
                    <Menu.Menu>
                      <Menu.Item
                        name="contact"
                        active={activeItem === 'contact'}
                        onClick={this.handleItemClick}
                      >
                        <FormattedMessage id="app.components.Profile.contact" />
                      </Menu.Item>
                    </Menu.Menu>
                  </Menu.Item>
                  <Menu.Item>
                    <Menu.Menu>
                      <Menu.Item
                        name="location"
                        active={activeItem === 'location'}
                        onClick={this.handleItemClick}
                      >
                        <FormattedMessage id="app.components.Profile.location" />
                      </Menu.Item>
                    </Menu.Menu>
                  </Menu.Item>
                  <Menu.Item>
                    <Menu.Menu>
                      <Menu.Item
                        name="availability"
                        active={activeItem === 'availability'}
                        onClick={this.handleItemClick}
                      >
                        <FormattedMessage id="app.components.Profile.availability" />
                      </Menu.Item>
                    </Menu.Menu>
                  </Menu.Item>
                  <Menu.Item>
                    <Menu.Menu>
                      <Menu.Item
                        name="conditions"
                        active={activeItem === 'conditions'}
                        onClick={this.handleItemClick}
                      >
                        <FormattedMessage id="app.components.Profile.conditions" />
                      </Menu.Item>
                    </Menu.Menu>
                  </Menu.Item>
                  <Menu.Item>
                    <Menu.Menu>
                      <Menu.Item
                        name="link"
                        active={activeItem === 'link'}
                        onClick={this.handleItemClick}
                      >
                        <FormattedMessage id="app.components.Profile.link" />
                      </Menu.Item>
                    </Menu.Menu>
                  </Menu.Item>
                  <Menu.Item />
                </Menu>
              </Grid.Column>
              <Grid.Column width={9}>
                <Header
                  style={{
                    marginTop: '3rem',
                    color: '#88DF88',
                    fontSize: '1.3em',
                  }}
                >
                  {activeItem}
                </Header>
                {activeItem === 'scope' && <p>{scope}</p>}
                {activeItem === 'scope' &&
                  !scope && (
                    <p>
                    <FormattedMessage id="app.components.Profile.writeScope" />
                    </p>
                  )}
                {activeItem === 'contact' && <p>{contact}</p>}
                {activeItem === 'contact' &&
                  !contact && (
                  <p>
                      <FormattedMessage id="app.components.Profile.writeContact" />
                  </p>
                )}
                {activeItem === 'location' && <p>{location}</p>}
                {activeItem === 'location' &&
                  !location && (
                    <p>
                      <FormattedMessage id="app.components.Profile.writeLocation" />
                  </p>
                )}
                {activeItem === 'location' &&
                  this.state.mode !== 'edit' && (
                  <LeafletMap addMarker={false} latlng={this.state.latlng} />
                  )}
                {activeItem === 'location' &&
                  this.state.latlng[0] === '' &&
                  this.state.latlng[1] === '' && (
                  <p>
                    <FormattedMessage id="noLocation" />
                    </p>
                  )}
                {activeItem === 'availability' && <p>{availability}</p>}
                {activeItem === 'availability' &&
                  !availability && (
                  <p>
                      <FormattedMessage id="app.components.Profile.writeAvailability" />
                  </p>
                )}
                {activeItem === 'conditions' && <p>{conditions}</p>}
                {activeItem === 'conditions' &&
                  !conditions && (
                    <p>
                    <FormattedMessage id="app.components.Profile.writeConditions" />
                    </p>
                  )}
                {activeItem === 'link' && <p>{link}</p>}
                {activeItem === 'link' &&
                  !link && (
                  <p>
                    <FormattedMessage id="app.components.Profile.writeLink" />
                    </p>
                  )}
                {activeItem === 'scope' &&
                  this.state.mode === 'edit' && (
                    <TextArea
                      style={{ minHeight: 100 }}
                    name="description"
                      type="text"
                    placeholder="Enter your description..."
                    autoFocus
                      value={scope}
                    onChange={e => this.setState({ scope: e.target.value })}
                    />
                  )}
                {activeItem === 'contact' &&
                  this.state.mode === 'edit' && (
                    <TextArea
                    style={{ minHeight: 100 }}
                      placeholder="Enter your contact..."
                      name="description"
                      type="text"
                      autoFocus
                      value={contact}
                      onChange={e => this.setState({ contact: e.target.value })}
                    />
                )}
                {activeItem === 'location' &&
                  this.state.mode === 'edit' && (
                    <div>
                      <Form.Input
                        fluid
                        placeholder="Location"
                      name="location"
                      type="text"
                        icon="map marker alternate"
                        autoFocus
                        iconPosition="left"
                      required
                        value={location}
                        onChange={e =>
                          this.setState({ location: e.target.value })
                        }
                    />
                    <Form>
                        <Form.Field>
                        <FormattedMessage id="app.components.AfterSignup.locationType" />
                          : <b>{this.state.value}</b>
                        </Form.Field>
                        <Form.Field>
                          <Radio
                            label="Use current location"
                          name="radioGroup"
                          value="current"
                            checked={this.state.value === 'current'}
                            onChange={(e, { value }) => {
                            this.setState({ value });
                            if (navigator.geolocation) {
                              const myPromise = new Promise(
                                (resolve, reject) => {
                                    this.setState({ locationLoading: true });
                                    navigator.geolocation.getCurrentPosition(
                                    position => resolve(position),
                                  );
                                },
                              );
                                myPromise.then(position =>
                                this.setState({
                                  locationLoading: false,
                                  latlng: [
                                    position.coords.latitude,
                                    position.coords.longitude,
                                  ],
                                }),
                              );
                              }
                            }}
                          />
                          <Loader
                            size="mini"
                            active={this.state.locationLoading}
                            inline
                          />
                      </Form.Field>
                        <Form.Field>
                          <Radio
                          label="Use custom location"
                            name="radioGroup"
                            value="custom"
                          checked={this.state.value === 'custom'}
                            onChange={(e, { value }) =>
                            this.setState({ value })
                          }
                        />
                      </Form.Field>
                    </Form>
                    {this.state.value && (
                      <Map
                          center={this.state.latlng}
                          zoom={13}
                          onClick={e => {
                            console.log(e);
                          this.setState({
                            latlng: [e.latlng.lat, e.latlng.lng],
                            });
                          }}
                        >
                          <Loader active={this.state.locationLoading} />
                          <TileLayer
                          attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                            url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
                          />
                        <Marker position={this.state.latlng}>
                          <Popup>
                            <FormattedMessage id="app.components.YourLocation" />
                            </Popup>
                        </Marker>
                        </Map>
                      )}
                    </div>
                  )}
                {activeItem === 'availability' &&
                  this.state.mode === 'edit' && (
                  <TextArea
                      style={{ minHeight: 100 }}
                      placeholder="Enter your availability..."
                    name="description"
                      type="text"
                      autoFocus
                    value={availability}
                      onChange={e =>
                        this.setState({ availability: e.target.value })
                      }
                    />
                  )}
                {activeItem === 'conditions' &&
                  this.state.mode === 'edit' && (
                    <TextArea
                      style={{ minHeight: 100 }}
                    placeholder="Enter your conditions..."
                    name="description"
                      type="text"
                      autoFocus
                      value={conditions}
                      onChange={e =>
                      this.setState({ conditions: e.target.value })
                    }
                  />
                  )}
                {activeItem === 'link' &&
                  this.state.mode === 'edit' && (
                    <TextArea
                    style={{ minHeight: 100 }}
                    placeholder="Enter your link..."
                      name="description"
                    type="text"
                    autoFocus
                      value={link}
                    onChange={e => this.setState({ link: e.target.value })}
                    />
                  )}
                <br />

                {this.state.editable && (
                  <Button
                    style={{ marginBottom: '5rem' }}
                    onClick={() => this.setState({ mode: 'edit' })}
                  >
                    <FormattedMessage id="app.components.Profile.edit" />
                  </Button>
                )}
                {this.state.mode === 'edit' && (
                  <Button onClick={this.handleSave}>
                    <FormattedMessage id="app.components.Profile.save" />
                  </Button>
                )}
              </Grid.Column>
              <Grid.Column width={2} />
            </Grid.Row>
          </Grid>
        ) : (
          <Header
            textAlign="center"
            size="huge"
            style={{ marginBottom: '33rem', marginTop: '10rem' }}
          >
            <FormattedMessage id="app.components.Profile.noProfile" />
          </Header>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  location: state.get('route').get('location'),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default injectIntl(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Profile),
);
