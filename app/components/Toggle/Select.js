import styled from 'styled-components';

const Select = styled.select`
  line-height: 1em;
  background-color: lightgray;
  margin: 20px;
`;

export default Select;
